const models = require('../../database/models/index.js')
const axios = require('axios')
const { v4: uuidv4 } = require('uuid')

const Cars = models.cars
const HOST = process.env.HOSTNAME || 'http://127.0.0.1:5000'

class CarsApiController {
    static handleGet = async (req, res) => {
        const { size_name = 'all', model = '', id = null } = req.query
        // initial query and join
        const query = {
            where: {},
            include: [{
                    model: models.size,
                    attributes: ['size_name'],
                }
            ]
        }
        // add filter by id if supplied id
        if (id){
            query.where = { ...query.where, id }
        }

        // add filter by size if supplid size
        if (size_name && size_name !== 'all'){
            query.include[0].where = { size_name: size_name.charAt(0).toUpperCase() + size_name.slice(1) }
        }

        //  add filter by model if supplied model
        if (model){
            query.where = { ...query.where, model }
        }

        // query data from database
        try{
            const car = await Cars.findAll(query)
            res.status(200).send({ success: true, results: car })
        } catch(err){
            res.status(500).send({ success: false, results: [] })
        }
    }


    static handleGetOne = async (req, res) => {
        const query = {
            where: { 
                id: req.params.id
            },
            include: [{
                    model: models.size,
                    attributes: ['size_name'],
                }
            ]
        }
        try{
            const car = await Cars.findOne(query)

            res.status(200).send({ success: true, results: car })
        } catch(err){
            res.status(500).send({ success: false, results: [] })
        }
    }

    static handlePost = async (req, res) => {
        // parsing data from request body
        const { model, rent_per_day, size, image } = req.body

        // generate random id
        const id = uuidv4()

        try{
            // get the sizeCode from size API
            const response = await axios.get(`${HOST}/api/v1/size`, { params: { size_name: size }})
            
            const size_code = response.data.results[0].code
            if (size_code){
                // create new record
                const newCar = await Cars.create({
                    id,
                    model,
                    size_code,
                    rent_per_day,
                    image,
                    available_at: '2022-03-23T15:49:05.563Z',
                    created_at: new Date(),
                    updated_at: new Date()
                })

                res.status(201).send({ success: true, results: [newCar] })
            } else {
                res.status(404).send({ success: false, results: [] })
            }
        } catch(err){
            res.status(500).send({ success: false, results: [] })
        }
    }
    static handlePut = async (req, res) => {
        // parsing data from request body
        const { model, rent_per_day, image, size, id } = req.body

        try{
            // getting sizeCode from size API
            const response = await axios.get(`${HOST}/api/v1/size`, { params: { size_name: size }})
            const size_code = response.data.results[0].code
            if (size_code){
                // updating the record based on id
                const updatedCar = await Cars.update({
                    model,
                    size_code,
                    rent_per_day,
                    image,
                    updated_at: new Date()
                }, {
                    where: {
                        id
                    }
                })
                res.status(200).send({ success: true, results: updatedCar })
            } else {
                res.status(404).send({ success: false, results: []})
            }
            
        } catch(err){
            res.status(500).send({ success: false, results: []})
        }
    }

    static handleDelete = async (req, res) => {
        // parsing data from request body
        const { id } = req.params
        console.log(id)

        try{
            // deleting the record
            const deletedData = await Cars.destroy({
                where: { id }
            })
            
            res.send({ success: true, results: deletedData })
        } catch (err){
            res.status(500).send({ success: true, results: [] })
        }
    }
}


module.exports = CarsApiController