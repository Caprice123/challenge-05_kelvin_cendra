const models = require('../../database/models/index.js')
const Size = models.size

class SizeApiController {
    static handleGet = async (req, res) => {
        
        // cleaning the query for the database
        try{
            const query = req.query
            // querying from the database
            const size =  await Size.findAll({
                where: query
            })
            res.status(200).send({ success: true, results: size })
            
        } catch(err){
            res.status(500).send({ success: false, results: [] })
        }
        
    }

}
module.exports =  SizeApiController