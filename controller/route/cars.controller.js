const { fetchData, fetchDataOne } = require('../../helpers/fetch.js')
const { renderView } = require('../../helpers/view.js')

const dotenv = require('dotenv')

dotenv.config()
const HOSTNAME = process.env.HOSTNAME || 'http://127.0.0.1:5000'
class CarRoute{
    static handleListCar = async (req, res) => {
        // parsing the flash message
        const [add = null] = req.flash("add")
        const [del = null] = req.flash("del")

        try{
            // get all available Size from size API
            const { results: availableSizes } = await fetchData({
                url: `${HOSTNAME}/api/v1/size`
            })
            
            // render view
            const view = {
                path: 'carsList.html',
                args: {
                    user: "Unis Badri",
                    availableSizes,
                    add,
                    del
                }
            }
            renderView(res, view.path, view.args)
        } catch(err){
            res.status(500).send({ message : "Internal Server Error" })
        }
    }

    static handleAddCarPage = async (req, res) => {
        try{
            // get all available Size from size API
            const { results: availableSizes } = await fetchData({
                url: `${HOSTNAME}/api/v1/size`
            })

            // render view
            const view = {
                path: 'carsAdd.html',
                args: {
                    user: "Unis Badri",
                    add: true,
                    initialData: null,
                    availableSizes
                }
            }
            renderView(res, view.path, view.args)
        } catch(err){
            res.status(500).send({ message : "Internal Server Error" })
        }
    }

    static handleUpdateCarPage = async (req, res) => {        
        try{
            // get original data that will be updated for the initial data
            const { results } = await fetchDataOne({
                url: `${HOSTNAME}/api/v1/cars/${req.params.id}`
            })

            // get all available size from size API
            const { results: availableSizes } = await fetchData({
                url: `${HOSTNAME}/api/v1/size`
            })

            // render view
            const view = {
                path: 'carsUpdate.html',
                args: {
                    user: "Unis Badri",
                    add: false,
                    initialData: results,
                    availableSizes
                }
            }
            renderView(res, view.path, view.args)
            
            
        } catch(err){
            res.status(500).send({ message : "Internal Server Error" })
        }
    }
}

module.exports =  CarRoute
