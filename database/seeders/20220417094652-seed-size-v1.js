'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('sizes', [
      {
        code: 'S',
        size_name: 'Small',
        created_at: new Date(),
        updated_at: new Date()
      }, {
        code: 'M',
        size_name: 'Medium',
        created_at: new Date(),
        updated_at: new Date()
      }, {
        code: 'L',
        size_name: 'Large',
        created_at: new Date(),
        updated_at: new Date()
      }, 
    ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('sizes', null, {});
  }
};
