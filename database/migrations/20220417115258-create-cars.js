'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('cars', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      model: {
        type: Sequelize.STRING,
        allowNull: false
      },
      image:{
        type: Sequelize.STRING,
        allowNull: false
      },
      rent_per_day: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      available_at: {
        type: Sequelize.DATE
      },
      size_code: {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
          model: 'sizes',
          key: 'code'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('cars');
  }
};