'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Cars.belongsTo(models.size, {
        foreignKey: 'size_code'
      })
    }
  }
  Cars.init({
    model: { type: DataTypes.STRING, allowNull: false},
    image:{ type: DataTypes.STRING, allowNull: false },
    rent_per_day: { type: DataTypes.INTEGER, allowNull: false },
    available_at: { type: DataTypes.DATE },
    size_code: { type: DataTypes.STRING, allowNull: false, },
    created_at: {allowNull: false,type: DataTypes.DATE},
    updated_at: {allowNull: false,type: DataTypes.DATE}
  }, {
    sequelize,
    modelName: 'cars',
    timestamps: false,
    underscored: true
  });
  return Cars;
};