const { Router } = require('express')
const SizeApiController = require('../controller/api/size.controller.js')

const router = Router()


router.get('/', SizeApiController.handleGet)
module.exports = router

