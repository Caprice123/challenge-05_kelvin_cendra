const { Router } = require('express')
const CarApiController = require('../controller/api/cars.controller.js')

const router = Router()

router.get('/', CarApiController.handleGet)
        .get('/:id', CarApiController.handleGetOne)
        .post('/', CarApiController.handlePost)
        .put('/', CarApiController.handlePut)
        .delete('/:id', CarApiController.handleDelete)

module.exports = router

