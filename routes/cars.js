const { Router } = require('express')
const CarRoute = require('../controller/route/cars.controller.js')
const CarService = require('../services/cars.services.js')

const router = Router()

router.get('/list', CarRoute.handleListCar)

router.get('/list/add', CarRoute.handleAddCarPage)
        .post('/list/add', CarService.handleAddCar)

router.get('/list/update/:id', CarRoute.handleUpdateCarPage)
        .post('/list/update/:id', CarService.handleUpdateCar)

router.get('/list/delete/:id', CarService.handleDeleteCar)

module.exports = router
