
const axios = require('axios')

const fetchData = async (restURI) => {
    const { url, params = {} } = restURI
    try{
        const response = await axios.get(url, { params })
        return response.data
    } catch (err){
        throw new Error(err.message)
    }
}

const fetchDataOne = async (restURI) => {
    const { url } = restURI
    try{
        const response = await axios.get(url)
        return response.data
    } catch (err){
        throw new Error(err.message)
    }
}

const postData = async (restURI) => {
    const { url, payload = {} } = restURI
    try{
        const response = await axios.post(url, payload)
        return response.data
    } catch(err){
        throw new Error(err.message)
    }
}

const deleteData = async (restURI) => {
    const { url } = restURI
    try{
        const response = await axios.delete(url)
        return response.data
    } catch(err){
        throw new Error(err.message)
    }
}

const putData = async (restURI) => {
    const { url, payload = {} } = restURI
    try{
        const response = await axios.put(url, payload)
        return response.data
    } catch(err){
        throw new Error(err.message)
    }
}

module.exports = { fetchData, fetchDataOne, postData, putData, deleteData }