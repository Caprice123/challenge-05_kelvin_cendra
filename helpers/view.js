
const pejs = require('pejs')

const views = pejs({
    watch: true
})

const renderView = (res, path, args) => {
    views.render(path, args, (err, result) => {
        if (err) {
            console.log(err)
            res.status(500).send({ message: "Internal Server Error" })
        }
        res.send(result)
    })
}

module.exports = { renderView }
