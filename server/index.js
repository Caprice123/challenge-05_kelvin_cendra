const fileUpload = require('express-fileupload')
const flash = require('connect-flash')
const session = require('express-session')
const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const ejs = require('ejs')

const CarApi = require('../api/cars.js')
const SizeApi = require('../api/size.js')
const CarRoute = require('../routes/cars.js')

dotenv.config()

const app = express()
const PORT = process.env.PORT || 5000

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())
app.use(flash())
app.use(session({
    secret: "123",
    saveUninitialized: false,
    resave: false
}))
app.use(fileUpload());
app.use(express.static('./public'));

app.engine('html', ejs.render)
app.set("view engine", 'html')

app.use('/cars', CarRoute)
app.use('/api/v1/cars', CarApi)
app.use('/api/v1/size', SizeApi)

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})
