const { deleteData, fetchData, postData, putData, fetchDataOne } = require('../helpers/fetch.js')
const { removeFile, saveFile, checkMultipleFile } = require('../helpers/file.js')
const dotenv = require('dotenv')

dotenv.config()
const HOSTNAME = process.env.HOSTNAME || 'http://127.0.0.1:5000'

class CarService{
    static handleAddCar = async (req, res) => {
        // parsing data from input text and file from input file
        const { name, rent, size } = req.body
        const { image } = req.files

        try{
            const imageName = checkMultipleFile(image.name)
            // posting data to save to the database
            await postData({
                url: `${HOSTNAME}/api/v1/cars`,
                payload: {
                    model: name,
                    rent_per_day: Number(rent.replaceAll("Rp.", "").replaceAll(",", "").trim()),
                    size,
                    image: imageName
                }
            })
            // saving the file
            saveFile(image, imageName)

            // back to list car
            req.flash("add", true)
            res.redirect('/cars/list')
        } catch(err){
            res.status(500).send({ message : "Internal Server Error" })
        }

    }

    static handleDeleteCar = async (req, res) => {
        // get the id that will be deleted from url
        const { id } = req.params

        try{
            // get the original data
            const { results } = await fetchData({
                url: `${HOSTNAME}/api/v1/cars`,
                params: req.params
            })

            // deleting data in database
            await deleteData({
                url: `${HOSTNAME}/api/v1/cars/${id}`,
            })
            
            // removing the file
            removeFile(results[0].image)

            // back to page list car
            req.flash("del", true)
            res.redirect('/cars/list')

        } catch (err){
            res.status(500).send({ message : "Internal Server Error" })
        }
    }
    
    static handleUpdateCar = async (req, res) => {
        // parsing data from input text and file from input file
        const { image } = req.files
        const { name, rent, size } = req.body

        try{
            // get the original data
            const { results } = await fetchDataOne({
                url: `${HOSTNAME}/api/v1/cars`,
                id: req.params.id
            })

            removeFile(results[0].image)
            
            const imageName = checkMultipleFile(image.name)
            // updating data
            await putData({
                url: `${HOSTNAME}/api/v1/cars`,
                payload: {
                    id: req.params.id,
                    model: name.trim(),
                    rent_per_day: Number(rent.replaceAll("Rp.", "").replaceAll(",", "").trim()),
                    size,
                    image: imageName
                }
            })

            // remove the file if the new image is not the same as the original image
            let originalImage = results[0].image
            originalImage = originalImage.split("/")[originalImage.split("/").length - 1]
    
            if (originalImage != image.name){
                saveFile(image, imageName)
            }
            
            // redirect to the list car page
            req.flash("add", true)
            res.redirect("/cars/list")
            
        } catch(err){
            res.status(500).send({ message : "Internal Server Error" })
        }
    }
}

module.exports = CarService
