# Car Management Dashboard
Car Management Dashboard is a system that allows admin user to add, edit and delete some cars. 

Technologies used: Express, Node.js, PostgreSQL

## How to run
First we need to export the environment of the system. By default, the environment is development

```bash
export NODE_ENV=test
```

Second, we need to download all the dependencies of the project

```bash
npm install
```

Then, we can start to change the database config file in database/config/config.json and change the username, password and database name

```bash
nano database/config/config.json
```

Then, change the username, password and database name
```bash
{
  "development": {
    "username": "XXXXXX",
    "password": "XXXXXX",
    "database": "cars_development",
    "host": "localhost",
    "dialect": "postgres",
    "logging": false
  },
  "test": {
    "username": "XXXXXX",
    "password": "XXXXXX",
    "database": "cars_test",
    "host": "localhost",
    "dialect": "postgres"
  },
  "production": {
    "username": "XXXXXX",
    "password": "XXXXXX",
    "database": "cars_prod",
    "host": "localhost",
    "dialect": "postgres"
  }
}

```

Then, run the migration

```bash
npx migrate
```

Then, create the seeders

```bash
npx seed
```

Last, start the server and server can run normally.

```bash
npm start
```

## Endpoints

### Endpoints Routes
GET /cars/list -> Rendering a Car List Page

GET /cars/list/add -> Rendering a Car Add Page

POST /cars/list/add -> Handling form POST for creating a car

GET /cars/list/update/:id -> Rendering a Car Update Page

POST /cars/list/update/:id -> Handling form POST for updating a specific car

GET /cars/list/delete/:id -> Handling a delete action from Car List Page


### Endpoints REST API
GET /api/v1/cars -> Getting all cars

GET /api/v1/cars/:id -> Getting a specific car by id

POST /api/v1/cars -> Creating a car

PUT /api/v1/cars -> Update a car

DELETE /api/v1/cars/:id -> Deleting a specific car by id

GET /api/v1/size -> Getting all available car size


## Directory Structure

```
.
├── api
│   ├── cars.js
│   └── size.js
├── controllers
│   ├── api
│   │   ├── cars.controller.js
│   │   └── size.controller.js
│   └── route
│       └── cars.controller.js
├── database
│   ├── config
│   │   └── config.json
│   ├── migrations
│   │   ├── 20220417115219-create-size.js
│   │   └── 20220417115258-create-cars.js
│   ├── models
│   │   ├── cars.js
│   │   ├── index.js
│   │   └── size.js
│   └── seeders
│       ├── 20220417094652-seed-size-v1.js
│       └── 20220417094752-seed-cars-v1.js
├── helpers
│   ├── fetch.js
│   ├── file.js
│   └── view.js
├── public
│   ├── css
│   │   ├── actionButton.css
│   │   ├── dashboard.css
│   │   ├── flash.css
│   │   ├── main.css
│   │   ├── mutationForm.css
│   │   ├── navbar.css
│   │   ├── popup.css
│   │   ├── sidebar.css
│   │   └── sidebarNavbar.css
│   ├── images
│   │   ├── imagebeepbeep.svg
│   │   └── image_car.css
│   ├── js
│   │   ├── dashboard.js
│   │   ├── delete.js
│   │   ├── filter.js
│   │   ├── listCar.js
│   │   ├── mutationForm.js
│   │   └── navbar.js
│   └── uploads
├── routes
│   └── cars.js
├── server
│   └── index.js
├── services
│   └── cars.services.js
├── views
│   ├── partials
│   │   ├── actionButtons.html
│   │   ├── cardCollection.html
│   │   ├── filter.html
│   │   ├── flash.html
│   │   ├── location.html
│   │   ├── mutationForm.html
│   │   ├── navbar.html
│   │   ├── popup.html
│   │   ├── sidebar.html
│   │   └── sidebarNavbar.html
│   ├── carsAdd.html
│   ├── carsList.html
│   ├── carsUpdate.html
│   └── templates.html
├── .gitignore
├── .sequelizerc
├── package.json
└── README.md
```

## ERD
![Entity Relationship Diagram](/public/images/ERD Diagram.png)

