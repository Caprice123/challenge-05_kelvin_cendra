class Delete{
    static #deleteButtons = null
    static #confirmationButton = document.querySelector(".confirmation-delete-button")
    
    static refresh = () => {
        this.#deleteButtons = document.querySelectorAll(".delete-button")
        this.#deleteButtons.forEach(
            deleteButton => deleteButton.onclick = this.#update
        )
    }

    static #update = (e) => {
        const target = e.currentTarget
        this.#confirmationButton.href = `/cars/list/delete/${target.dataset.id}`
    }
}

