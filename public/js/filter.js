class Filter{
    constructor(){
        this.query = {
            sizeName: "",
            model: ""
        }
        this.cardContainer = document.querySelector(".card-collection")
        this.filterButtons = document.querySelectorAll(".filter-button")

        this.searchInput = document.querySelector("input.model")
        this.searchButton = document.querySelector(".search-model-button")
        
        this.filterData()
    }

    init(){
        this.filterButtons.forEach(
            button => button.onclick = this.updateFilter
        )

        this.searchButton.onclick = this.updateFilter
    }

    updateFilter = (e) => {
        const target = e.currentTarget

        if (target.classList.contains("filter-button")){
            if (this.query.sizeName !== target.dataset.filter){
                this.query.sizeName = target.dataset.filter
                this.filterButtons.forEach(
                    button => button.classList.remove("selected")
                )
                target.classList.add("selected")
                this.filterData()
            }
        } else if (target.classList.contains("search-model-button")){
            if (this.query.model !== this.searchInput.value){
                this.query.model = this.searchInput.value
                this.filterData()
            }
        }
    }

    filterData = async () => {
        this.cardContainer.innerHTML = '<div class="spinner"></div>'

        const { success, results } = await this.fetchData()
        if (success){
            this.updateView(results)
        }
    }

    updateView = (cars) => {
        console.log(cars)
        this.cardContainer.innerHTML = ''
        ListCar.init(cars)
        ListCar.list.forEach(car => {
            const html = car.render()
            this.cardContainer.insertAdjacentHTML("beforeend", html)
        })
        Delete.refresh()
    }

    fetchData = async () => {
        const response = await axios.get("/api/v1/cars", { params: this.query })
        const data = response.data
        return data
    }
}
